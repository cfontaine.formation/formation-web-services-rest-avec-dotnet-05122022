﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _01_webapi.Repository
{
    public class ProduitRepository : GenericRepository<Produit>
    {
        private ProduitContext dataContext;

        public ProduitRepository(ProduitContext dataContext) :base(dataContext)
        {
            this.dataContext = dataContext;
        }

        public IEnumerable<Produit> FindAll()
        {
            return dataContext.Produits
                .Include("Marque")
                .Include("Categorie")
                .ToList();
        }

        public Produit FindById(long id)
        {
            return dataContext.Produits.Include(i => i.Categorie)
              .Include(i => i.Marque)
              .FirstOrDefault(x => x.Id == id); ;
        }
    }
}