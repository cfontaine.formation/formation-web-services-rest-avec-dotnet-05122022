﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web;
using System.Web.Http;

namespace _01_webapi.Repository
{
    public class AdresseRepository : GenericRepository<Adresse>
    {
        public AdresseRepository(ProduitContext dataContext) : base(dataContext)
        {
        }


        // TODO Ajouter des méthodes spécifique à Adresse 
    }
}
