﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_webapi.Repository
{
    public interface IUserRepository
    {
        List<User> FindAll();
        void Save(User user);
        User Find(long id);
        void Update(User user);
        void Delete(long id);
        User FindByMail(string email);

        List<User> FindByNameOrEmail(string search);

    }
}
