﻿using _01_webapi.Models;
using System.Linq;

namespace _01_webapi.Repository
{
    public class CategorieRepository : GenericRepository<Categorie>
    {
        private ProduitContext db;
        public CategorieRepository(ProduitContext dataContext) : base(dataContext)
        {
            db = dataContext;
        }

        public Categorie FindByName(string name)
        {
            return db.Categories .Where(s => s.Nom.Equals(name)).FirstOrDefault();
        }
    }
}