﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _01_webapi.Repository
{
    public class ProduitContext : DbContext
    {
        public ProduitContext() : base("name=ProduitContext")
        {
        }

        public DbSet<Produit> Produits { get; set; }

        public DbSet<Adresse> Adresses { get; set; }

        public DbSet<Fournisseur> Fournisseurs { get; set; }

        public DbSet<Societe> Societes { get; set; }

        public DbSet<Categorie> Categories { get; set; }

        public DbSet<User> Users { get; set; }

    }
}