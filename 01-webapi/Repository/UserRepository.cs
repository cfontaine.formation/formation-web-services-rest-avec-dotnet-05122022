﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _01_webapi.Repository
{
    public class UserRepository : IUserRepository
    {
        private ProduitContext db;

        public UserRepository(ProduitContext db)
        {
            this.db = db;
        }

        public void Delete(long id)
        {
            db.Users.Remove(db.Users.Find(id));
            db.SaveChanges();
        }

        public User Find(long id)
        {
            return db.Users.AsNoTracking().SingleOrDefault(x => x.Id == id);
        }

        public List<User> FindAll()
        {
            return db.Users.AsNoTracking().ToList();
        }

        public User FindByMail(string email)
        {
            return db.Users.Where(u => email.Equals(u.Email)).SingleOrDefault();
        }

        public List<User> FindByNameOrEmail(string search)
        {
            var req = from u in db.Users.AsNoTracking()
                      where u.Nom.ToLower().Contains(search) || u.Email.ToLower().Contains(search)
                      select u;
            return req.ToList();
        }

        public void Save(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void Update(User user)
        {
            db.Entry(user).State=EntityState.Modified;
            db.SaveChanges();
        }
    }
}