﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_webapi.Repository
{
    public interface IGenericRepository<T> where T : AbstractEntity
    {
        IQueryable<T> Collection();
        T FindById(long id);
        List<T> FindAll();
        void Insert(T t);
        void Update(T t);
        void Delete(long id);
        void Commit();
    }
}
