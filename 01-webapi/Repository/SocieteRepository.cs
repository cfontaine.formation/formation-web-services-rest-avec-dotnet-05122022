﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.Repository
{
    public class SocieteRepository : GenericRepository<Societe>
    {
        private ProduitContext dataContext;
        public SocieteRepository(ProduitContext dataContext) : base(dataContext)
        {
           this.dataContext = dataContext;
        }

        public IEnumerable<Societe> FindAll()
        {
            return dataContext.Societes.Include("CurrentAdresse").ToList();
        }
    }
}