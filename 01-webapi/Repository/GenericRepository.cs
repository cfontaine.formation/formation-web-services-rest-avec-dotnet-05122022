﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace _01_webapi.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : AbstractEntity
    {
        protected ProduitContext DataContext;
        protected DbSet<T> dbSet;

        public GenericRepository(ProduitContext dataContext)
        {
            DataContext = dataContext;
            dbSet=DataContext.Set<T>();
        }

        public IQueryable<T> Collection()
        {
            return dbSet;
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }

        public void Delete(long id)
        {
            var t = FindById(id);
            if (DataContext.Entry(t).State == EntityState.Detached)
            {
                dbSet.Attach(t);
            }
            dbSet.Remove(t);
        }

        public List<T> FindAll()
        {
            return dbSet.ToList();
        }

        public T FindById(long id)
        {
            return dbSet.Find(id);
        }

        public void Insert(T t)
        {
            dbSet.Add(t);
        }

        public void Update(T t)
        {
            dbSet.Attach(t);
            DataContext.Entry(t).State = EntityState.Modified;
        }
    }
}