﻿namespace _01_webapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ajout_catagorie : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.categories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nom = c.String(),
                        date_creation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Produits", "Categorie_Id", c => c.Long());
            CreateIndex("dbo.Produits", "Categorie_Id");
            AddForeignKey("dbo.Produits", "Categorie_Id", "dbo.categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Produits", "Categorie_Id", "dbo.categories");
            DropIndex("dbo.Produits", new[] { "Categorie_Id" });
            DropColumn("dbo.Produits", "Categorie_Id");
            DropTable("dbo.categories");
        }
    }
}
