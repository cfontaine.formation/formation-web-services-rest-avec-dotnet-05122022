﻿namespace _01_webapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.adresses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Rue = c.String(),
                        Ville = c.String(),
                        code_postal = c.String(),
                        Pays = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.fournisseurs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Produits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(),
                        Prix = c.Double(nullable: false),
                        date_sortie = c.DateTime(nullable: false),
                        Marque_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.societes", t => t.Marque_Id)
                .Index(t => t.Marque_Id);
            
            CreateTable(
                "dbo.societes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nom = c.String(),
                        CurrentAdresse_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.adresses", t => t.CurrentAdresse_Id)
                .Index(t => t.CurrentAdresse_Id);
            
            CreateTable(
                "dbo.ProduitFournisseurs",
                c => new
                    {
                        Produit_Id = c.Long(nullable: false),
                        Fournisseur_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Produit_Id, t.Fournisseur_Id })
                .ForeignKey("dbo.Produits", t => t.Produit_Id, cascadeDelete: true)
                .ForeignKey("dbo.fournisseurs", t => t.Fournisseur_Id, cascadeDelete: true)
                .Index(t => t.Produit_Id)
                .Index(t => t.Fournisseur_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Produits", "Marque_Id", "dbo.societes");
            DropForeignKey("dbo.societes", "CurrentAdresse_Id", "dbo.adresses");
            DropForeignKey("dbo.ProduitFournisseurs", "Fournisseur_Id", "dbo.fournisseurs");
            DropForeignKey("dbo.ProduitFournisseurs", "Produit_Id", "dbo.Produits");
            DropIndex("dbo.ProduitFournisseurs", new[] { "Fournisseur_Id" });
            DropIndex("dbo.ProduitFournisseurs", new[] { "Produit_Id" });
            DropIndex("dbo.societes", new[] { "CurrentAdresse_Id" });
            DropIndex("dbo.Produits", new[] { "Marque_Id" });
            DropTable("dbo.ProduitFournisseurs");
            DropTable("dbo.societes");
            DropTable("dbo.Produits");
            DropTable("dbo.fournisseurs");
            DropTable("dbo.adresses");
        }
    }
}
