﻿namespace _01_webapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ajout_user : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nom = c.String(nullable: false),
                        Email = c.String(maxLength: 150),
                        Password = c.String(),
                        creation_date = c.DateTime(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.users", new[] { "Email" });
            DropTable("dbo.users");
        }
    }
}
