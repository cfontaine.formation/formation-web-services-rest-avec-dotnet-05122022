﻿namespace _01_webapi.Migrations
{
    using _01_webapi.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<_01_webapi.Repository.ProduitContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_01_webapi.Repository.ProduitContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            //List<Adresse> list = new List<Adresse>
            //{
            //    new Adresse(){CodePostal="59000",Pays="France",Rue="1, rue esquermoise",Ville="Lille"},
            //    new Adresse(){CodePostal="59000",Pays="France",Rue="46 rue des canonniers",Ville="Lille"}
            //};
            //context.Adresses.AddRange(list);
            //Societe s1 = new Societe() { CurrentAdresse = list[0], Nom = "marqueA" };
            //context.Societes.AddOrUpdate(s1);
        }
    }
}
