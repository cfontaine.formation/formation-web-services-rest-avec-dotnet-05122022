﻿using _01_webapi.Models;
using _01_webapi.Repository;
using _01_webapi.Services;
using _01_webapi.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace _01_webapi.Filters
{
    public class JWTFilterAttribute : ActionFilterAttribute
    {
        private ProduitContext db = new ProduitContext();
        private UserService _userService;

        public JWTFilterAttribute()
        {
            _userService = new UserService(new UserRepository(db));
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var tokenWithBearer = actionContext.Request.Headers.Authorization;
            if (tokenWithBearer == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            string token= tokenWithBearer.ToString().Substring(7);
            string userToken=TokenManager.ValidateToken(token);
            if(userToken== null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            else
            {
                User u = _userService.FindByEmail(userToken);
                if(u == null || (u!=null && !u.Email.Equals(userToken)))
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                else
                {
                    actionContext.Request.Properties.Add("userReq", u);
                }
            }
        }
    }
}