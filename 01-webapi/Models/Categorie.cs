﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _01_webapi.Models
{
    [Table("categories")]
    public class Categorie : AbstractEntity
    {
        public string Nom {get; set;}

        [Column("date_creation")]
        public string DateCreation { get; set; }
    }
}