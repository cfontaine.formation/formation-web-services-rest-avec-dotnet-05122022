﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _01_webapi.Models
{
    [Table("fournisseurs")]
    public class Fournisseur :AbstractEntity
    {
        public string Nom { get;set; }
        public List<Produit> Produits { get; set; }
    }
}