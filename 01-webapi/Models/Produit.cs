﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _01_webapi.Models
{
    [Table("Produits")]
    public class Produit :AbstractEntity
    {
        public string Description { get; set; }

        public double Prix { get; set; }

        [Column("date_sortie")]
        public DateTime DateSortie { get; set; }

        public Societe Marque { get; set; }


        public Categorie Categorie { get; set; }

        public List<Fournisseur> Fournisseurs { get; set; }
    }
}