﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _01_webapi.Models
{
    [Table("societes")]
    public class Societe :AbstractEntity
    {
        public string Nom { get; set; }

        [Column("current_adresse")]
        public Adresse CurrentAdresse { get; set; }

        [JsonIgnore]
        public List<Produit> Produits { get; set; }

        public override string ToString()
        {
            return $"{Nom} {CurrentAdresse}";
        }


    }
}