﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _01_webapi.Models
{
    [Table("adresses")]
    public class Adresse :AbstractEntity
    {
 
        public string Rue { get; set; }

        public string Ville { get; set; }

        [Column("code_postal")]
        public string CodePostal { get; set; }

        public string Pays { get; set; }

        public override string ToString()
        {
            return $"{Rue} {CodePostal} {Ville} {Pays}";
        }
    }
}