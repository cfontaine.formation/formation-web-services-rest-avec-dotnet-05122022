﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _01_webapi.Models
{
    public enum UserRole
    {
        Customer,
        SalePerson,
        Manager
    }
    [Table("users")]
    public class User :AbstractEntity
    {
        [Required]
        [MinLength(2)]
        public string Nom { get; set; }

        [MaxLength(150)]
        [Index(IsUnique =true)]
        public string Email { get; set; }

        public string Password { get; set; }

        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        public UserRole Role { get; set; }

        public User()
        {
            CreationDate= DateTime.Now;
        }
    }
}