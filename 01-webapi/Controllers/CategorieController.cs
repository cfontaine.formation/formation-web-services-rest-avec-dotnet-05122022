﻿using _01_webapi.Models;
using _01_webapi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _01_webapi.Controllers
{
    [RoutePrefix("api/categorie")]
    public class CategorieController : ApiController
    {
        private CategorieRepository Repository;
        private ProduitContext db = new ProduitContext();

        public CategorieController()
        {
            Repository = new CategorieRepository(db);
        }

        [Route("")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Repository.FindAll());
        }

        [Route("{id:long}")]
        public HttpResponseMessage Get(long id)
        {
            Categorie cat=Repository.FindById(id);
            if (cat == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"La catégorie {id} n'existe pas");
            }
            return Request.CreateResponse(HttpStatusCode.OK, cat);
        }

        [Route("{name:alpha}")]
        public HttpResponseMessage Get(string name)
        {
            Categorie cat = Repository.FindByName(name);
            if (cat == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"La catégorie {name} n'existe pas");
            }
            return Request.CreateResponse(HttpStatusCode.OK, cat);
        }

        [Route("{id:long}")]
        public HttpResponseMessage Delete(long id)
        {
            try
            {
                Repository.Delete(id);
                Repository.Commit();
                return Request.CreateResponse(HttpStatusCode.OK, $"Categorie avec Id={id} a été supprimée");
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Adresse avec Id={id} n'est pas trouvé");
            }
        }

        [Route("")]
        public void Post([FromUri]Categorie cat)
        {
            Repository.Insert(cat);
            Repository.Commit();
        }


        [Route("{id:long}")]
        public void Put(long id, Categorie cat)
        {
            cat.Id = id;
            Repository.Update(cat);
            Repository.Commit();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
