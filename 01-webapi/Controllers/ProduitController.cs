﻿using _01_webapi.Models;
using _01_webapi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _01_webapi.Controllers
{
    [RoutePrefix("api/produit")]
    public class ProduitController : ApiController
    {
        private ProduitRepository Repository;
        private ProduitContext db = new ProduitContext();

        public ProduitController()
        {
            Repository = new ProduitRepository(db);
        }

        [Route("")]
        public IEnumerable<Produit> Get()
        {
            return  Repository.FindAll();
        }

        [Route("{id:long}")]
        public HttpResponseMessage Get(long id)
        {
            Produit p = Repository.FindById(id);
            if (p == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Le Produit {id} n'existe pas");
            }
            return Request.CreateResponse(HttpStatusCode.OK, p);
        }

        [Route("{id:long}")]
        public HttpResponseMessage Delete(long id)
        {
            try
            {
                Repository.Delete(id);
                Repository.Commit();
                return Request.CreateResponse(HttpStatusCode.OK, $"Le Produit avec Id={id} a été supprimée");
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Le Produit avec Id={id} n'a pas trouvé");
            }
        }

        [Route("")]
        public void Post(Produit p)
        {
            Repository.Insert(p);
            Repository.Commit();
        }


        [Route("{id:long}")]
        public void Put(long id, Produit p)
        {
            p.Id = id;
            Repository.Update(p);
            Repository.Commit();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
