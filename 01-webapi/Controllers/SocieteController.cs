﻿using _01_webapi.Models;
using _01_webapi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _01_webapi.Controllers
{

    [RoutePrefix("api/societe")]
    public class SocieteController : ApiController
    {
        private ProduitContext db = new ProduitContext();
        private SocieteRepository Repository;

        public SocieteController()
        {
            Repository = new SocieteRepository(db);
        }

        [Route("")]
        public IEnumerable<Societe> Get()
        {
            return Repository.FindAll();
        }

        //[Route("{id}")]
        //public Societe Get(long id)
        //{
        //    Societe rep=Repository.FindById(id);
        //    if (rep == null)
        //    {
        //        var expRep = new HttpResponseMessage(HttpStatusCode.NotFound)
        //        {
        //            Content = new StringContent($"Société avec l'id={id} n'existe pas")
        //        };
        //        throw new HttpResponseException(expRep);
        //    }
        //    return rep;
        //}


        [Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            Societe rep = Repository.FindById(id);
            if (rep == null)
            {
                var expRep = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent($"Société avec l'id={id} n'existe pas")
                };
                throw new HttpResponseException(expRep);
            }
            return Ok(rep);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
