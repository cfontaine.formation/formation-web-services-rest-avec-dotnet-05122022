﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using _01_webapi.Repository;
using _01_webapi.Services;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Web.Http;

namespace _01_webapi.Controllers
{
    [RoutePrefix("api/adresse")]
    public class AdresseController : ApiController
    {
        private AdresseService AdrService;
        private ProduitContext db = new ProduitContext();

        public AdresseController()
        {
            AdrService =new AdresseService( new AdresseRepository(db));
        }

        [Route("")]
        public IEnumerable<AdresseDto> Get()
        {
           return AdrService.FindAll();
        }


        [Route("{id}")]
        public HttpResponseMessage Get(long id)
        {
            AdresseDto adr = AdrService.FindById(id);
            if (adr == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Adresse avec Id={id} n'est pas trouvé");
            }
            return Request.CreateResponse(HttpStatusCode.OK, adr);
        }

        [Route("")]
        public HttpResponseMessage Post(AdresseDto adr)
        {
            if (ModelState.IsValid)
            {
                //AdrService.Insert(adr);
                //AdrService.Commit();
                AdrService.Save(ref adr);
                HttpResponseMessage response=new HttpResponseMessage(HttpStatusCode.OK);
                response.Headers.Location= new Uri($"https://localhost:44301/api/adresse/{adr.Id}");
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        /// <summary>
        /// Supprimer une adresse
        /// </summary>
        /// <param name="id">id de l'adresse à supprimer</param>
        /// <response code="200"> OK</response>
        /// /// <response code="400"> OK</response>
        [Route("{id}")]
        public HttpResponseMessage Delete(long id)
        {
            try
            {
                AdrService.Delete(id);
                AdrService.Commit();
                return Request.CreateResponse(HttpStatusCode.OK, $"Adresse avec Id={id} a été supprimée");
            }
            catch(Exception )
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Adresse avec Id={id} n'est pas trouvé");
            }
        }

        [Route("{id}")]
        public void Put(long id,AdresseDto adr)
        {
            adr.Id = id;
            AdrService.Update(adr);
            AdrService.Commit();
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
