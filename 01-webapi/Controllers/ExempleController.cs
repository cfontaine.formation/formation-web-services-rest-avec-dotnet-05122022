﻿using _01_webapi.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _01_webapi.Controllers
{
    [JWTFilter]
    [RoutePrefix("api/exemple")]
    public class ExempleController : ApiController
    {
        //[HttpGet]
        //[ActionName("testapi")]
        //public string Message()
        //{
        //    return "Hello World";
        //}

        //[HttpGet]
        //public string Message2()
        //{
        //    return "Hello World x2";
        //}

        //[AcceptVerbs("POST","PUT")]
        //[ActionName("testapi")]
        //public string MessagePost()
        //{
        //    return "Hello Post";
        //}

        //[NonAction]
        //public string GetData()
        //{
        //    return "getdata";
        //}

        // Routage d'attribut

        /*        [Route("api/marque/all")]
                public string GetExempleAttribut()
                {
                    return "Exemple attribut";
                }*/
        [HttpGet]
        [Route("all")]
        public string ExempleAttribut()
        {
            return "Exemple attribut";
        }

        [HttpGet]
        [Route("{numExemple}/add")]
        public string ExempleParamAttribut(int numExemple)
        {
            return "Exemple attribut param="+numExemple;
        }

        [HttpGet]
        [Route("~/api/other/all")]
        public string ExempleSansPrefix()
        {
            return "Exemple attribut Sans Prefix" ;
        }

        [HttpGet]
        [Route("paramoption/{numExemple?}")]
        public string ExempleParamAttributOption(int numExemple=42)
        {
            return "Exemple attribut option param=" + numExemple;
        }

        [HttpGet]
        [Route("valdefault/{numExemple:int=123}",Name ="valdefaut")]
        public string ExempleParamAttributDefaut(int numExemple)
        {
            return "Exemple attribut option param=" + numExemple;
        }


        [HttpGet]
        [Route("conflit/{num}")]
        public string ExempleConflit2(string num)
        {
            return "Exemple conflit val alpha=" + num;
        }

        [HttpGet]
        [Route("conflit/{numExemple:int:min(20)}")]
        public string ExempleConflit1(int numExemple)
        {
            return "Exemple conflit val nul=" + numExemple;
        }

        [HttpGet]
        [Route("notimplemented")]
        [NotImplExceptionFilter]
        public string TestFilter()
        {
            throw new NotImplementedException("la méthode n'est pas implément");

        }

    }
}
