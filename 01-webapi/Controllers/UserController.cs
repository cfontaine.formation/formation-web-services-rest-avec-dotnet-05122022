﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using _01_webapi.Repository;
using _01_webapi.Services;
using _01_webapi.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Permissions;
using System.Web.Http;
using System.Web.UI.WebControls;

namespace _01_webapi.Controllers
{
    [RoutePrefix ("api/user")]
    public class UserController : ApiController
    {
        private ProduitContext db = new ProduitContext();
        private IUserService service;

        public UserController()
        {
            service = new UserService(new UserRepository(db));
        }

        [Route("")]
        public IEnumerable<User> Get()
        {
            return service.FindAll();
        }

        [Route("{id}",Name="GetUserById")]
        public HttpResponseMessage Get(long id)
        {
            User user = service.Find(id);
            if (user == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"utilisateur id={id} inconnue");
            }
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [Route("")]
        public HttpResponseMessage Post(User u)
        {
/*            if(!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }*/
            service.Save(u);
            var rep= Request.CreateResponse(HttpStatusCode.OK,u);
    /*        Uri uri = new Uri(Url.Link("GetUserById", u.Id));
            rep.Headers.Location = uri;*/
            return rep;
        }

        [Route("{id}")]
        public void Put(long id, User u)
        {
            u.Id= id;
            service.Update(u);
        }

        [Route("{id}")]
        public void Delete(long id)
        {
            service.Remove(id);
        }
        [HttpPost]
        [Route("~/api/Login")]
        public HttpResponseMessage CheckLogin(UserLogin u)
        {
            try
            {
                User userDb = service.CheckLogin(u);
                string token = TokenManager.GenerateToken(u.Email);
                return Request.CreateResponse(HttpStatusCode.OK, token);
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,e.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            { 
                db.Dispose(); 
            }
            base.Dispose(disposing);
        }
    }
}
