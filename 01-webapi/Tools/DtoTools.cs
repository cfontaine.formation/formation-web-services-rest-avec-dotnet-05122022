﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using FastMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.Tools
{
    public static class DtoTools
    {

        public static T2 Convert<T1,T2>(T1 objToConvert)
        {
            // On ajoute les règles de mapping dto<=>entity

            TypeAdapterConfig<Adresse, AdresseDto>
                .NewConfig()
                .MapFrom(dest => dest.Id, src => src.Id)
                .MapFrom(dest => dest.Rue, src => src.Rue)
                .MapFrom(dest => dest.CodePostal, src => src.CodePostal)
                .MapFrom(dest => dest.Ville, src => src.Ville)
                .MapFrom(dest => dest.Pays, src => src.Pays);

            TypeAdapterConfig<AdresseDto, Adresse>
          .NewConfig()
          .MapFrom(dest => dest.Id, src => src.Id)
          .MapFrom(dest => dest.Rue, src => src.Rue)
          .MapFrom(dest => dest.CodePostal, src => src.CodePostal)
          .MapFrom(dest => dest.Ville, src => src.Ville)
          .MapFrom(dest => dest.Pays, src => src.Pays);

            return TypeAdapter.Adapt<T1, T2>(objToConvert);
        }
    }
}