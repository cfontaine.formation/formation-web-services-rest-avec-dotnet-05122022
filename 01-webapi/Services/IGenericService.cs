﻿using _01_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_webapi.Services
{
    public interface IGenericService<TEntity,TDto> where TEntity : AbstractEntity
    {
        IList<TDto> Collection();
        TDto FindById(long id);
        List<TDto> FindAll();
        void Insert(TDto t);
        void Update(TDto t);
        void Delete(long id);
        void Commit();

        void Save(ref TDto t);
    }
}

