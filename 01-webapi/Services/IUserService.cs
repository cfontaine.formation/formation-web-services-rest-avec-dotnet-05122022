﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _01_webapi.Services
{
    public interface IUserService
    {
        List<User> FindAll();
        void Save(User user);

        User Find(long id);

        void Update(User user);
        void Remove(long id);

        List<User> Search(string search);

        User FindByEmail(string email);

        User CheckLogin(UserLogin login);
    }
}
