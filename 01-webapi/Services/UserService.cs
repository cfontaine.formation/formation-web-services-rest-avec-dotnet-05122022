﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using _01_webapi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public User CheckLogin(UserLogin login)
        {
            User u=_userRepository.FindByMail(login.Email);
            if (u != null)
            {
                if(BCrypt.Net.BCrypt.Verify(login.Password,u.Password))
                {
                    return u;
                }
            }
            throw new Exception("Identifiants incorrects !");
        }

        public User Find(long id)
        {
            return _userRepository.Find(id);
        }

        public List<User> FindAll()
        {
            return _userRepository.FindAll();
        }

        public User FindByEmail(string email)
        {
            return _userRepository.FindByMail(email);
        }

        public void Remove(long id)
        {
            _userRepository.Delete(id);
        }

        public void Save(User user)
        {
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            _userRepository.Save(user);
        }

        public List<User> Search(string search)
        {
            search = search.Trim().ToLower();
            return _userRepository.FindByNameOrEmail(search);
        }

        public void Update(User user)
        {
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            _userRepository.Update(user);
        }
    }
}