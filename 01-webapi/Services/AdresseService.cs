﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using _01_webapi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.Services
{
    public class AdresseService : GenericService<Adresse, AdresseDto>, IAdresseService
    {
        public AdresseService(IGenericRepository<Adresse> genericRepository) : base(genericRepository)
        {
        }
    }
}