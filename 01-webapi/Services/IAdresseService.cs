﻿using _01_webapi.Models;
using _01_webapi.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_webapi.Services
{
    public interface IAdresseService : IGenericService<Adresse, AdresseDto>
    {
    }
}
