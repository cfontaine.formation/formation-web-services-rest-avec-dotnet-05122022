﻿using _01_webapi.Models;
using _01_webapi.Repository;
using _01_webapi.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.Services
{
    public class GenericService<TEntity, TDto> : IGenericService<TEntity, TDto> where TEntity : AbstractEntity
    {
        protected IGenericRepository<TEntity> genericRepository;

        public GenericService(IGenericRepository<TEntity> genericRepository)
        {
            this.genericRepository = genericRepository;
        }

        public IList<TDto> Collection()
        {
            List<TEntity> res = genericRepository.Collection().ToList();
            var resDto=DtoTools.Convert<List<TEntity>,List< TDto >> (res);
            return resDto;
        }

        public void Commit()
        {
            genericRepository.Commit();
        }

        public void Delete(long id)
        {
            genericRepository.Delete(id);
        }

        public List<TDto> FindAll()
        {
            var res = genericRepository.FindAll();
            var resDto = DtoTools.Convert<List<TEntity>, List<TDto>>(res);
            return resDto;
        }

        public TDto FindById(long id)
        {
            var res=genericRepository.FindById(id);
            return DtoTools.Convert<TEntity, TDto>(res);
        }

        public void Insert(TDto t)
        {
            var tObj = DtoTools.Convert<TDto, TEntity>(t);
            genericRepository.Insert(tObj);
        }

        public void Update(TDto t)
        {
            var tObj = DtoTools.Convert<TDto, TEntity>(t);
            genericRepository.Update(tObj);
        }

        public void Save(ref TDto t)
        {
            var tObj = DtoTools.Convert<TDto, TEntity>(t);
            genericRepository.Insert(tObj);
            genericRepository.Commit();
            t= DtoTools.Convert<TEntity, TDto>(tObj);
        }
    }
}