﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01_webapi.ModelsDto
{
    public class UserLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}