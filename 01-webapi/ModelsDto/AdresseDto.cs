﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _01_webapi.ModelsDto
{
    public class AdresseDto
    {
        public long Id { get; set; }

        [Required]
        public string Rue { get; set; }
        [Required]
        public string CodePostal { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Ville{get; set;}

        public string Pays { get; set; }

    }
}