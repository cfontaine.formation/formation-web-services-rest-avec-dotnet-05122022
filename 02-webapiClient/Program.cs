﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace _02_webapiClient
{
    internal class Program
    {
        static HttpClient Client = new HttpClient();
        static void Main(string[] args)
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task<Uri> PostAdresse(AdresseDto adresse)
        {
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/adresse", adresse);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        static async Task<AdresseDto> GetAdresse(string path)
        {
            AdresseDto adresse = null;
            HttpResponseMessage rep = await Client.GetAsync(path);
            if (rep.IsSuccessStatusCode)
            {
                adresse = await rep.Content.ReadAsAsync<AdresseDto>();
            }
            return adresse;
        }

        static async Task<HttpStatusCode> DeleteAdresse(long id)
        {
           HttpResponseMessage rep= await Client.DeleteAsync($"api/adresse/{id}");
            return rep.StatusCode;
        }

        static async Task<AdresseDto> PutAdresse(AdresseDto adresse)
        {
            HttpResponseMessage rep=await Client.PutAsJsonAsync<AdresseDto>($"api/adresse/{adresse.Id}", adresse);
            rep.EnsureSuccessStatusCode();
            AdresseDto adrRep = await rep.Content.ReadAsAsync<AdresseDto>();
            return adrRep;
        }

        static async Task RunAsync() {
            Client.BaseAddress = new Uri("https://localhost:44301/");
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json" )) ;

            AdresseDto adresse = new AdresseDto
            {
                Rue = "42, Boulevard Vincent Gache",
                Ville = "Nantes",
                CodePostal = "44000",
                Pays = "France"
            };
            try
            {
                var url = await PostAdresse(adresse);
                Console.WriteLine($"Post at {url.PathAndQuery}");
                adresse = await GetAdresse(url.PathAndQuery);
                Console.WriteLine(adresse);
                adresse.Rue = "42, Blv Vincent Gache";
                await PutAdresse(adresse);
                adresse = await GetAdresse(url.PathAndQuery);
                Console.WriteLine(adresse);
                var stcode= await DeleteAdresse(adresse.Id);
                Console.WriteLine(stcode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
        
    }
}
