﻿namespace _02_webapiClient
{
    public class AdresseDto
    {
        public long Id { get; set; }

        public string Rue { get; set; }

        public string CodePostal { get; set; }
        public string Ville{get; set;}

        public string Pays { get; set; }

        public override string ToString()
        {
            return $"{Id} {Rue} {CodePostal} {Ville} {Pays}";
        }
    }
}